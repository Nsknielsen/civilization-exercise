package src.common.util;

import src.common.game.Game;
import src.common.game.Player;
import src.common.world.City;
import src.common.world.Position;
import src.common.world.Tile;
import src.common.world.Unit;

import java.util.HashMap;

public class LogDecorator implements Game {

    public LogDecorator(Game game) {
        this.game = game;
    }

    private Game game;
    @Override
    public HashMap<Position, City> getCities() {
        return this.game.getCities();
    }

    @Override
    public Tile getTileAt(Position p) {
        return this.game.getTileAt(p);
    }

    @Override
    public Unit getUnitAt(Position p) {
        return this.game.getUnitAt(p);
    }

    @Override
    public City getCityAt(Position p) {
        return this.game.getCityAt(p);
    }

    @Override
    public Player getPlayerInTurn() {
        return this.game.getPlayerInTurn();
    }

    @Override
    public Player getWinner() {
        System.out.println(String.format("%s is the current winner", this.game.getWinner()));
        return this.game.getWinner();
    }

    @Override
    public int getAge() {
        return this.game.getAge();
    }

    @Override
    public boolean moveUnit(Position from, Position to) {
        System.out.println(String.format("%s moves %s from (%d, %d) to (%d, %d) // ", this.game.getPlayerInTurn(),
                this.game.getUnitAt(from).getTypeString(), from.getRow(), from.getColumn(), to.getRow(), to.getColumn()));
        return this.game.moveUnit(from, to);
    }

    @Override
    public void endOfTurn() {
        System.out.println(String.format("%s ends turn // ", this.game.getPlayerInTurn()));
        this.game.endOfTurn();
    }

    @Override
    public void changeWorkForceFocusInCityAt(Position p, String balance) {
        System.out.println(String.format("%s changes workforce focus in city at" +
                        "(%d, %d) to %s // ", this.game.getPlayerInTurn(), p.getColumn(), p.getRow(),
                game.getCityAt(p).getWorkforceFocus()));
        this.game.changeWorkForceFocusInCityAt(p, balance);
    }

    @Override
    public void changeProductionInCityAt(Position p, String unitType) {
        System.out.println(String.format("%s changes production in city at" +
                        "(%d, %d) to %s // ", this.game.getPlayerInTurn(), p.getColumn(), p.getRow(),
                game.getCityAt(p).getProduction()));
        this.game.changeProductionInCityAt(p, unitType);

    }

    @Override
    public void performUnitActionAt(Position p) {
        System.out.println(String.format("%s performs %s action at (%d, %d) // ", this.game.getPlayerInTurn(),
                this.game.getUnitAt(p).getTypeString(),
                p.getRow(), p.getColumn()));
        this.game.performUnitActionAt(p);
    }
}