package src.common.util;

import src.common.world.Position;

import java.util.ArrayList;
import java.util.Arrays;

public class SurroundingPositionsUtil {


    private SurroundingPositionsUtil() {
    }

    static public ArrayList<Position> listOfSurroundingPositions(int i, int j) {
        return new ArrayList<>(Arrays.asList(new Position(i - 1, j),
                new Position(i - 1, j + 1),
                new Position(i, j + 1),
                new Position(i + 1 , j+1),
                new Position(i+1, j),
                new Position(i+1, j-1),
                new Position(i, j-1),
                new Position(i-1, j-1)
        ));
    }
}
