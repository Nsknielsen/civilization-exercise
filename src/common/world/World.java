package src.common.world;

import java.util.HashMap;

public interface World {

    public void putUnits(HashMap<Position, Unit> newUnits);

    public Unit getUnitAt(Position p);

    public void putUnit(Position p, Unit u);
    
    public void setUnits(HashMap<Position, Unit> units);

    public HashMap<Position, Unit> getUnits();

    public void removeUnitAt(Position p);

    public City getCityAt(Position p);

    public void putCity(Position p, City c);

    public void setCities(HashMap<Position, City> cities);

    public HashMap<Position, City> getCities();

    public void removeCityAt(Position p);

    public Tile getTileAt(Position p);

    public void putTile(Position p, Tile t);

    public HashMap<Position, Tile> getTiles();

    public void setTiles(HashMap<Position, Tile> tiles);
}
