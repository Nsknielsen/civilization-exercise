package src.common.world;

import src.common.game.Player;

public class CityImpl implements City {

    private Player owner;

    private int treasury;

    private int food;

    private int population;

    private String unitBeingProduced;

    private String workforceFocus;

    public CityImpl(Player owner, int treasury, String unitBeingProduced, int food, int population, String workforceFocus){
        this.owner = owner;
        this.treasury = treasury;
        this.unitBeingProduced = unitBeingProduced;
        this.workforceFocus = workforceFocus;
        this.population = population;
        this.food = food;
    }

    @Override
    public int getFood() {
        return this.food;
    }

    @Override
    public int getPopulation() {
        return this.population;
    }

    public int getTreasury() {
        return treasury;
    }

    @Override
    public Player getOwner() {

        return this.owner;
    }

    @Override
    public int getSize() {
        return 1;
    }

    @Override
    public String getProduction() {
        return this.unitBeingProduced;
    }

    @Override
    public String getWorkforceFocus() {
        return this.workforceFocus;
    }
}
