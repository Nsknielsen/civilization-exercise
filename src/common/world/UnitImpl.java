package src.common.world;

import src.common.game.Player;

public class UnitImpl implements Unit {

    private String unitType;
    private Player owner;

    private int defense;
    private int attack;

    private int moveCount;
    public UnitImpl(String unitType, Player owner, int moveCount, int attack, int defense){
        this.unitType = unitType;
        this.owner = owner;
        this.moveCount = moveCount;
        this.attack = attack;
        this.defense = defense;

    }



    @Override
    public String getTypeString() {
        return this.unitType;
    }

    @Override
    public Player getOwner() {
        return this.owner;
    }

    @Override
    public int getMoveCount() {
        return this.moveCount;
    }

    @Override
    public int getDefensiveStrength() {
        return this.defense;
    }

    @Override
    public int getAttackingStrength() {
        return this.attack;
    }
}
