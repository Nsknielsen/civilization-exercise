package src.common.world;

import java.util.HashMap;

public class WorldImpl implements World {

    private HashMap<Position, Unit> units;

    private HashMap<Position, City> cities;

    private HashMap<Position, Tile> tiles;

    public WorldImpl(){this.units = new HashMap<>(); this.cities = new HashMap<>(); this.tiles = new HashMap<>();}


    @Override
    public void putUnits(HashMap<Position, Unit> newUnits) {
        this.units.putAll(newUnits);
    }

    @Override
    public Unit getUnitAt(Position p) {
        return this.units.get(p);
    }

    @Override
    public void putUnit(Position p, Unit u) {
        if (u != null){
            this.units.put(p, u);
        } else {
            this.units.remove(p);
        }
    }

    @Override
    public void setUnits(HashMap<Position, Unit> units) {
        this.units = units;
    }


    @Override
    public HashMap<Position, Unit> getUnits() {
        return this.units;
    }

    @Override
    public void removeUnitAt(Position p) {
        this.units.remove(p);
    }

    @Override
    public City getCityAt(Position p) {
        return this.cities.get(p);
    }

    @Override
    public void putCity(Position p, City c) {
        if (c != null){
            this.cities.put(p, c);
        }

    }

    @Override
    public void setCities(HashMap<Position, City> cities) {
     this.cities = cities;
    }

    @Override
    public HashMap<Position, City> getCities() {
        return this.cities;
    }

    @Override
    public void removeCityAt(Position p) {
    this.cities.remove(p);
    }

    @Override
    public Tile getTileAt(Position p) {
        return this.tiles.get(p) != null ? this.tiles.get(p) : new TileImpl(GameConstants.PLAINS);
    }

    @Override
    public void putTile(Position p, Tile t) {
    this.tiles.put(p, t);
    }

    @Override
    public HashMap<Position, Tile> getTiles() {
        return this.tiles;
    }

    @Override
    public void setTiles(HashMap<Position, Tile> tiles) {
        this.tiles = tiles;
    }

}
