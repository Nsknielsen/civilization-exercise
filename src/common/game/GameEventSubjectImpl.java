package src.common.game;

import src.common.world.City;
import src.common.world.Position;
import src.variant.win.WinStrategy;

import java.util.HashMap;

public class GameEventSubjectImpl implements GameEventSubject {

    private int gameAge;

    private HashMap<Position, City> cities;

    private HashMap<Player, Integer> attackerWins;

    private WinStrategy observer;
    private int rounds;

    public GameEventSubjectImpl(){
        this.attackerWins = new HashMap<>();
        this.attackerWins.put(Player.RED, 0);
        this.attackerWins.put(Player.BLUE, 0);
    }

    @Override
    public void incrementRounds() {
        this.rounds += 1;
    }

    @Override
    public int getRounds() {
        return this.rounds;
    }

    @Override
    public void updateGameAge(int newAge) {
        this.gameAge = newAge;
        notifyObserver();
    }

    @Override
    public int getGameAge() {
        return this.gameAge;
    }

    @Override
    public void updateCities(HashMap<Position, City> cities) {
        this.cities = cities;
        notifyObserver();
    }

    @Override
    public HashMap<Position, City> getCities() {
        return this.cities;
    }

    @Override
    public HashMap<Player, Integer> getAttackerWins() {
        return this.attackerWins;
    }

    @Override
    public void updateAttackerWins(Player attacker) {
        if (this.attackerWins.get(attacker) == null){
            this.attackerWins.put(attacker, 0);
        } else {
            this.attackerWins.put(attacker, this.attackerWins.get(attacker) + 1);
        }
        notifyObserver();
    }

    @Override
    public void addObserver(WinStrategy observer) {
        this.observer = observer;
    }

    @Override
    public void notifyObserver() {
        this.observer.update();
    }
}
