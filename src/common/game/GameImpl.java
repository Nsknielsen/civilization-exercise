package src.common.game;

import src.common.builder.CityBuilder;
import src.common.builder.CityBuilderImpl;
import src.common.builder.UnitBuilder;
import src.common.builder.UnitBuilderImpl;
import src.common.dto.ResourceUpdateImpl;
import src.common.dto.UnitEventObject;
import src.common.world.*;
import src.variant.age.AgeStrategy;
import src.variant.attack.AttackStrategy;
import src.variant.factory.CivFactory;
import src.variant.population.PopulationStrategy;
import src.variant.unitAction.UnitActionStrategy;
import src.variant.win.WinStrategy;
import src.variant.workforce.WorkforceStrategy;
import src.variant.worldGeneration.WorldGenerationStrategy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static src.common.util.SurroundingPositionsUtil.listOfSurroundingPositions;

/** Skeleton implementation of HotCiv.
 
   This source code is from the book 
     "Flexible, Reliable Software:
       Using Patterns and Agile Development"
     published by CRC Press.
   Author: 
     Henrik B Christensen 
     Department of Computer Science
     Aarhus University
   
   Please visit http://www.baerbak.com/ for further information.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
 
       http://www.apache.org/licenses/LICENSE-2.0
 
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/

public class GameImpl implements Game {

  private Player playerInTurn;

  private int age;

  private final Player[] players;

  private final int boardSize;

  private final HashMap<String, Integer> unitPrices;

  private int playersDoneThisRound;

  private final WinStrategy winStrategy;
  private final AgeStrategy ageStrategy;

  private final UnitActionStrategy unitActionStrategy;

  private final WorldGenerationStrategy worldGenerationStrategy;

  private final UnitBuilder unitBuilder;

  private final CityBuilder cityBuilder;
  private final World world;

  private final AttackStrategy attackStrategy;

  private GameEventSubject gameEventSubject;

  private WorkforceStrategy workforceStrategy;

  private PopulationStrategy populationStrategy;

  public GameImpl(CivFactory factory) {
    this.populationStrategy = factory.populationStrategy();
    this.workforceStrategy = factory.workforceStrategy();
    this.unitActionStrategy = factory.unitActionStrategy();
    this.ageStrategy = factory.ageStrategy();
    this.worldGenerationStrategy = factory.worldGenerationStrategy();
    this.attackStrategy = factory.attackStrategy();
    this.players = new Player[]{Player.RED, Player.BLUE};
    this.playersDoneThisRound = 0;
    this.playerInTurn = Player.RED;
    this.age = -4000;
    this.boardSize = 16;
    this.world = this.worldGenerationStrategy.generateWorld();
    this.unitPrices = setUnitPrices();
    this.unitBuilder = new UnitBuilderImpl();
    this.cityBuilder = new CityBuilderImpl();
    this.gameEventSubject = new GameEventSubjectImpl();
    this.winStrategy = factory.winStrategy();
    this.gameEventSubject.addObserver(this.winStrategy);
    this.winStrategy.addEventSubject(this.gameEventSubject);
    this.gameEventSubject.updateGameAge(this.age);
    this.gameEventSubject.updateCities(getCities());
  }

  private HashMap<String, Integer> setUnitPrices(){
    HashMap<String, Integer> unitPrices = new HashMap<>();
    unitPrices.put(GameConstants.ARCHER, 10);
    unitPrices.put(GameConstants.LEGION, 15);
    unitPrices.put(GameConstants.SETTLER, 30);
    return unitPrices;
  }

  public HashMap<Position, City> getCities() {
    return this.world.getCities();
  }

  public Tile getTileAt(Position p ) {
    return this.world.getTileAt(p);
  }

  public Unit getUnitAt( Position p ) {
    return this.world.getUnitAt(p);}

  public HashMap<Position, Unit> getUnits(){
    return this.world.getUnits();
  }

  public City getCityAt(Position p ) {
    return this.world.getCityAt(p);
  }

  public Player getPlayerInTurn() { return this.playerInTurn; }
  public Player getWinner() {
    return this.winStrategy.getWinner();
  }
  public int getAge() { return this.age; }

  public void endOfTurn() {
    this.playersDoneThisRound += 1;
    this.playerInTurn = playerInTurn == Player.RED ? Player.BLUE : Player.RED;
    if (this.playersDoneThisRound == this.players.length){
      endOfRound();}
  }

  private void endOfRound() {
    this.world.setCities(this.workforceStrategy.updateResources(this.world));
    this.world.setCities(this.populationStrategy.updatePopulations(getCities()));
    this.world.setUnits(restoreUnitMovementPoints());
    this.world.putUnits(produceUnits());
    this.age = increaseGameAge();
    this.gameEventSubject.updateGameAge(this.age);
    this.gameEventSubject.incrementRounds();
    this.playersDoneThisRound = 0;
  }

  private HashMap<Position, Unit> produceUnits() {
    HashMap<Position, Unit> newUnits = new HashMap<>();
    for (Map.Entry<Position, City> entry: getCities().entrySet()){
      if (entry.getValue() != null){
        newUnits.put(placeNewUnit(entry.getKey()), produceUnit(entry.getKey()));
      }
    }
    return newUnits;
  }

  private Unit produceUnit(Position p) {
    City city = getCityAt(p);
    int unitPrice = this.unitPrices.get(city.getProduction());
    if (city.getTreasury() >= unitPrice){
      City c = getCityAt(p);
      this.world.putCity(p, this.cityBuilder.build(c, new ResourceUpdateImpl(- unitPrice, 0)
              ));
      return this.unitBuilder.build(city);
    }
    return null;
  }

  private Position placeNewUnit(Position p) {
    if (getUnitAt(p) == null){
      return p;
    }
    ArrayList<Position> surroundingPositions = listOfSurroundingPositions(p.getRow(), p.getColumn());
    return surroundingPositions.stream().filter(sp ->
              getUnitAt(sp) == null && !unitOwnerIsInTurn(sp) && !isOutsideBoard(sp) && !isNoMovementTile(sp))
              .findFirst().orElse(placeNewUnit(surroundingPositions.get(0)));
  }

  public void changeWorkForceFocusInCityAt( Position p, String workforceFocus ) {
    this.world.putCity(p, this.workforceStrategy.changeWorkForceFocusInCity(getCityAt(p), workforceFocus));
  }
  public void changeProductionInCityAt( Position p, String unitType ) {
    if (getCityAt(p) != null){
      City c = getCityAt(p);
      this.world.putCity(p, this.cityBuilder.build(c, unitType));
    }
  }

  private int increaseGameAge() {
    return this.ageStrategy.increaseGameAge(this.getAge());
  }

  public boolean moveUnit( Position from, Position to ) {
    if (!validateMove(from, to)){
      return false;
    }
    if (getUnitAt(to) == null){
      moveNoAttack(from, to);
    } else {
      moveWithAttack(from, to);
    }
    return true;
  }

  private void moveWithAttack(Position from, Position to) {
    if (this.attackStrategy.resolveAttack(this.world, from, to)){
      attackerWins(from, to);
    } else {
      this.world.removeUnitAt(from);
    }
  }

  private void attackerWins(Position from, Position to){
    this.gameEventSubject.updateAttackerWins(getUnitAt(from).getOwner());
    this.world.putUnit(to, unitBuilder.build(getUnitAt(from), 0));
    this.world.removeUnitAt(from);
    if (getCityAt(to) != null){
      this.world.putCity(to, moveIntoCity(to));
    }
  }

  private void moveNoAttack(Position from, Position to) {
    this.world.putUnit(to, unitBuilder.build(getUnitAt(from), 0));
    this.world.removeUnitAt(from);
    if (getCityAt(to) != null){
      this.world.putCity(to, moveIntoCity(to));
    }
  }

  public City moveIntoCity(Position p){
    if (getCityAt(p).getOwner() != getUnitAt(p).getOwner()){
      City newCity = cityBuilder.build(getCityAt(p),getUnitAt(p).getOwner());
      this.gameEventSubject.updateCities(getCities());
      return newCity;
    }
    return getCityAt(p);
  }

  private boolean validateMove(Position from, Position to) {
    if (!this.unitActionStrategy.isAllowedToMove(getUnitAt(from)));
    if (isOutsideBoard(to) || isNoMovementTile(to) || unitAtPositionIsNull(from) ){
      return false;
    }
    if (!isValidDistance(from, to) || unitHasNoMovement(from) || !unitOwnerIsInTurn(from)){
      return false;
    }
    if (getUnitAt(to) != null && unitOwnerIsInTurn(to)){
      return false;
    }
    return true;
  }

  public void performUnitActionAt( Position p ) {
    UnitEventObject result = this.unitActionStrategy.performUnitAction(getCityAt(p), getUnitAt(p));
      this.world.putCity(p, result.getCity());
      this.world.putUnit(p, result.getUnit());
    }

  private HashMap<Position, Unit>  restoreUnitMovementPoints(){
    HashMap<Position, Unit> updatedUnits = new HashMap<>();
    for (Map.Entry<Position, Unit> entry : getUnits().entrySet()){
      if (entry.getValue() != null){
        updatedUnits.put(entry.getKey(), this.unitBuilder.build(entry.getValue(), 1));
      }
    }
    return updatedUnits;
  }
  private boolean isOutsideBoard(Position to ){
    return to.getRow() < 0 || to.getColumn() >= this.boardSize || to.getColumn() < 0 || to.getColumn() >= this.boardSize;
  }

  private boolean isNoMovementTile(Position to){
    return getTileAt(to).getTypeString() == GameConstants.OCEANS || getTileAt(to).getTypeString() == GameConstants.MOUNTAINS;
  }

  private boolean unitHasNoMovement(Position p){
    return getUnitAt(p).getMoveCount() < 1;
  }

  private boolean unitAtPositionIsNull(Position p){
    return getUnitAt(p) == null;
  }

  private boolean isValidDistance(Position from, Position to){
    boolean result = false;
    if ((to.getRow() == from.getRow() - 1 || to.getRow() == from.getRow() + 1) && to.getColumn() == from.getColumn()){
      result = true;
    }
    if ((to.getColumn() == from.getColumn() - 1 || to.getColumn() == from.getColumn() + 1) && to.getRow() == from.getRow()){
      result = true;
    }
    if ((to.getColumn() == from.getColumn() - 1 ||to.getColumn() == from.getColumn() + 1) && (to.getRow() == from.getRow() - 1 || to.getRow() == from.getRow() + 1)){
      result = true;
    }
    return result;
  }

  private boolean unitOwnerIsInTurn(Position position){
    return getUnitAt(position) != null ? getPlayerInTurn() == getUnitAt(position).getOwner() : false;}
}
