package src.common.game;

import src.common.world.City;
import src.common.world.Position;
import src.variant.win.WinStrategy;

import java.util.HashMap;

public interface GameEventSubject {

    public void incrementRounds();

    public int getRounds();
    public void updateGameAge(int newAge);

    public int getGameAge();

    public void updateCities(HashMap<Position, City> cities);

    public HashMap<Position, City> getCities();

    public HashMap<Player, Integer> getAttackerWins();

    public void updateAttackerWins(Player attacker);

    public void addObserver(WinStrategy observer);

    public void notifyObserver();


}
