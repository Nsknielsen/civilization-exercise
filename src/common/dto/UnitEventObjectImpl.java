package src.common.dto;

import src.common.world.City;
import src.common.world.Unit;

public class UnitEventObjectImpl implements UnitEventObject {

    private Unit unit;

    private City city;

    public UnitEventObjectImpl(City city, Unit unit) {
        this.unit = unit;
        this.city = city;
    }

    @Override
    public Unit getUnit() {
        return this.unit;
    }

    @Override
    public City getCity() {
        return this.city;
    }
}
