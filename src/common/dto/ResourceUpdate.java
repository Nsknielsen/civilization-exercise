package src.common.dto;

public interface ResourceUpdate {

    public int getFood();

    public int getProduction();
}
