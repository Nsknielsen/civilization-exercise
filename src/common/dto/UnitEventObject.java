package src.common.dto;

import src.common.world.City;
import src.common.world.Unit;

public interface UnitEventObject {

    public Unit getUnit();

    public City getCity();
}
