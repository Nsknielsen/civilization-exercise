package src.common.dto;

public class ResourceUpdateImpl implements ResourceUpdate {
    public ResourceUpdateImpl(int production, int food) {
        this.food = food;
        this.production = production;
    }

    private int food;

    private int production;


    @Override
    public int getFood() {
        return food;
    }

    @Override
    public int getProduction() {
        return production;
    }
}
