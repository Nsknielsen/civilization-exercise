package src.common.builder;

import src.common.dto.ResourceUpdate;
import src.common.world.City;
import src.common.game.Player;

public interface CityBuilder {

    public City build(City c, ResourceUpdate resourceUpdate);

    public City build(City c, String unitType);

    public City build(City c, Player owner);

    public City build(City c);


}
