package src.common.builder;

import src.common.dto.ResourceUpdate;
import src.common.world.City;
import src.common.world.CityImpl;
import src.common.world.GameConstants;
import src.common.game.Player;

public class CityBuilderImpl implements CityBuilder {

    @Override
    public City build(City c, ResourceUpdate resourceUpdate) {
        return new CityImpl(c.getOwner(),
                c.getTreasury() + resourceUpdate.getProduction(),
                c.getProduction(), c.getFood() + resourceUpdate.getFood(), c.getPopulation(), c.getWorkforceFocus());
    }

    @Override
    public City build(City c, String unitType) {
        return new CityImpl(c.getOwner(),
                c.getTreasury(), unitType, c.getFood(), c.getPopulation(), c.getWorkforceFocus());
    }

    @Override
    public City build(City c, Player owner) {
        return new CityImpl(owner, 0, GameConstants.ARCHER, 0, c.getPopulation(), GameConstants.productionFocus);
    }

    @Override
    public City build(City c) {
        return new CityImpl(c.getOwner(), c.getTreasury(), c.getProduction(), 0, c.getPopulation() + 1, c.getWorkforceFocus());

    }
}
