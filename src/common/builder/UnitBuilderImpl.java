package src.common.builder;

import src.common.world.City;
import src.common.world.GameConstants;
import src.common.world.Unit;
import src.common.world.UnitImpl;

import java.util.HashMap;

public class UnitBuilderImpl implements UnitBuilder {

    private HashMap<String, int[]> baseUnitStats;

    public UnitBuilderImpl(){
        this.baseUnitStats = new HashMap<>();
        this.baseUnitStats.put(GameConstants.ARCHER, new int[]{2, 3});
        this.baseUnitStats.put(GameConstants.LEGION, new int[]{4, 2});
        this.baseUnitStats.put(GameConstants.SETTLER, new int[]{0, 3});
    }

    @Override
    public Unit build(City c) {
        return new UnitImpl(c.getProduction(), c.getOwner(), 1,
                this.baseUnitStats.get(c.getProduction())[0], this.baseUnitStats.get(c.getProduction())[1]);
    }

    @Override
    public Unit build(Unit u, int moveCount) {
        return new UnitImpl(u.getTypeString(), u.getOwner(), moveCount, u.getAttackingStrength(), u.getDefensiveStrength());
    }
}
