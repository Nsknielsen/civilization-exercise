package src.common.builder;

import src.common.world.City;
import src.common.world.Unit;

public interface UnitBuilder {

    public Unit build(City c);

    public Unit build(Unit u, int moveCount);
}
