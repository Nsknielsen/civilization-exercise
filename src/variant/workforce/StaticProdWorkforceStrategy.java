package src.variant.workforce;

import src.common.builder.CityBuilder;
import src.common.builder.CityBuilderImpl;
import src.common.dto.ResourceUpdateImpl;
import src.common.world.City;
import src.common.world.Position;
import src.common.world.World;

import java.util.HashMap;
import java.util.Map;


public class StaticProdWorkforceStrategy implements WorkforceStrategy {
    private CityBuilder cityBuilder = new CityBuilderImpl();
    @Override
    public City changeWorkForceFocusInCity(City c, String workforceFocus) {
        return c;
    }

    @Override
    public HashMap<Position, City> updateResources(World world) {

        HashMap <Position, City> updatedCities = world.getCities();
        for (Map.Entry<Position, City> entry : updatedCities.entrySet()) {
            City c = entry.getValue();
            updatedCities.put(entry.getKey(), this.cityBuilder.build(c, new ResourceUpdateImpl(6, 0)));
        }
        return  updatedCities;
    }

}
