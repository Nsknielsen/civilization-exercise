package src.variant.workforce.resourceFocus;

import src.common.dto.ResourceUpdateImpl;
import src.common.world.GameConstants;

import java.util.HashMap;

public class ProductionFocusStrategy implements ResourceFocusStrategy {

    private HashMap<String, Integer> tileProductionValues;

    public ProductionFocusStrategy(){
        this.tileProductionValues = setTileProductionValues();
    }

    private HashMap<String, Integer> setTileProductionValues() {
        HashMap<String, Integer> values = new HashMap<>();
        values.put(GameConstants.PLAINS, 0);
        values.put(GameConstants.OCEANS, 0);
        values.put(GameConstants.FOREST, 3);
        values.put(GameConstants.MOUNTAINS, 1);
        values.put(GameConstants.HILLS, 2);
        return values;
    }

    @Override
    public HashMap<String, Integer> getTileResourceValues() {
        return tileProductionValues;
    }

    @Override
    public ResourceUpdateImpl getResourceEarnings(int focusEarnings) {
        return new ResourceUpdateImpl(focusEarnings, 1);
    }
}
