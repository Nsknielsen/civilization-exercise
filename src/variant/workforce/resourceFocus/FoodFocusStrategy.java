package src.variant.workforce.resourceFocus;

import src.common.dto.ResourceUpdateImpl;
import src.common.world.GameConstants;

import java.util.HashMap;

public class FoodFocusStrategy implements ResourceFocusStrategy {

    private HashMap<String, Integer> tileFoodValues;


    public FoodFocusStrategy(){
        this.tileFoodValues = setTileFoodValues();
    }
    private HashMap<String, Integer> setTileFoodValues() {
        HashMap<String, Integer> values = new HashMap<>();
        values.put(GameConstants.PLAINS, 3);
        values.put(GameConstants.OCEANS, 1);
        values.put(GameConstants.FOREST, 0);
        values.put(GameConstants.MOUNTAINS, 0);
        values.put(GameConstants.HILLS, 0);
        return values;
    }
    @Override
    public HashMap<String, Integer> getTileResourceValues() {
        return tileFoodValues;
    }

    @Override
    public ResourceUpdateImpl getResourceEarnings(int focusEarnings) {
        return new ResourceUpdateImpl(1, focusEarnings);
    }
}
