package src.variant.workforce.resourceFocus;

import src.common.dto.ResourceUpdateImpl;

import java.util.HashMap;

public interface ResourceFocusStrategy {

    public HashMap<String, Integer> getTileResourceValues();

    public ResourceUpdateImpl getResourceEarnings(int focusEarnings);
}
