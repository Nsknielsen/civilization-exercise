package src.variant.workforce;

import src.common.world.City;
import src.common.world.Position;
import src.common.world.World;

import java.util.HashMap;


public interface WorkforceStrategy {

    public City changeWorkForceFocusInCity(City c, String workforceFocus);


    public HashMap<Position, City> updateResources(World world);
}
