package src.variant.workforce;

import src.common.builder.CityBuilder;
import src.common.builder.CityBuilderImpl;
import src.common.dto.ResourceUpdate;
import src.common.world.*;
import src.variant.workforce.resourceFocus.FoodFocusStrategy;
import src.variant.workforce.resourceFocus.ProductionFocusStrategy;
import src.variant.workforce.resourceFocus.ResourceFocusStrategy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static src.common.util.SurroundingPositionsUtil.listOfSurroundingPositions;

public class VariedWorkforceStrategy implements WorkforceStrategy {

    private CityBuilder cityBuilder;

    private ResourceFocusStrategy foodEarningsStrategy;

    private ResourceFocusStrategy productionEarningsStrategy;

    private ResourceFocusStrategy currentState;


    public VariedWorkforceStrategy(){
        this.cityBuilder = new CityBuilderImpl();
        this.foodEarningsStrategy = new FoodFocusStrategy();
        this.productionEarningsStrategy = new ProductionFocusStrategy();
        this.currentState = null;
    }

    @Override
    public City changeWorkForceFocusInCity(City c, String workforceFocus) {
        return new CityImpl(c.getOwner(), c.getTreasury(),
                c.getProduction(), c.getFood(), c.getPopulation(), workforceFocus);
    }

    @Override
    public HashMap<Position, City> updateResources(World world) {
        HashMap <Position, City> updatedCities = world.getCities();
        for (Map.Entry<Position, City> entry : updatedCities.entrySet()){
            City c = entry.getValue();
            this.currentState = c.getWorkforceFocus().equals(GameConstants.productionFocus)
                    ? this.productionEarningsStrategy : this.foodEarningsStrategy;
            updatedCities.put(entry.getKey(), this.cityBuilder.build(c, getEarningsForCity(world, entry.getKey())));
        }

        return updatedCities;
    }

    private ResourceUpdate getEarningsForCity(World world, Position p){
        City c = world.getCityAt(p);
        int focusResourceSum = 1;
        if (c.getPopulation() > 1){
            int maxResourceFound = 0;
            ArrayList<Position> surroundingPositions = listOfSurroundingPositions(p.getRow(), p.getColumn());
            for (int i = 0; i < c.getPopulation() - 1; i++) {
                for (int j = 0; j < surroundingPositions.size(); j++) {
                    Position sp = surroundingPositions.get(j);
                    int resourceValueOfSp = this.currentState.getTileResourceValues().get(world.getTileAt(sp).getTypeString());
                    if ( resourceValueOfSp > maxResourceFound){
                        focusResourceSum += resourceValueOfSp;
                        surroundingPositions.remove(sp);
                        maxResourceFound = 0;
                        break;
                    }
                }
            }
        }
        return this.currentState.getResourceEarnings(focusResourceSum);
    }
}
