package src.variant.population;

import src.common.world.City;
import src.common.world.Position;

import java.util.HashMap;

public interface PopulationStrategy {

    public HashMap<Position, City> updatePopulations(HashMap<Position, City> cities);
}
