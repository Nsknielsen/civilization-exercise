package src.variant.population;

import src.common.builder.CityBuilder;
import src.common.builder.CityBuilderImpl;
import src.common.world.City;
import src.common.world.Position;

import java.util.HashMap;
import java.util.Map;

public class FoodPopulationStrategy implements PopulationStrategy {

    private CityBuilder cityBuilder;

    public FoodPopulationStrategy() {
        this.cityBuilder = new CityBuilderImpl();
    }


    @Override
    public HashMap<Position, City> updatePopulations(HashMap<Position, City> cities) {

        HashMap<Position, City> updatedCities = cities;
        for (Map.Entry<Position, City> entry : updatedCities.entrySet()){
            City c = entry.getValue();
            if (c.getFood() > 5 + c.getPopulation() * 3 && c.getPopulation() < 9){
                updatedCities.put(entry.getKey(), this.cityBuilder.build(c));
            }
        }
        return updatedCities;
    }
}
