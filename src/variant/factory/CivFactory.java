package src.variant.factory;

import src.variant.age.AgeStrategy;
import src.variant.attack.AttackStrategy;
import src.variant.population.PopulationStrategy;
import src.variant.unitAction.UnitActionStrategy;
import src.variant.win.WinStrategy;
import src.variant.workforce.WorkforceStrategy;
import src.variant.worldGeneration.WorldGenerationStrategy;

public interface CivFactory {

    public WorkforceStrategy workForceStrategy();

    public WinStrategy winStrategy();

    public AgeStrategy ageStrategy();

    public UnitActionStrategy unitActionStrategy();

    public WorldGenerationStrategy worldGenerationStrategy();

    public AttackStrategy attackStrategy();

    public PopulationStrategy populationStrategy();

    WorkforceStrategy workforceStrategy();
}
