package src.variant.factory;

import src.variant.age.AgeStrategy;
import src.variant.age.StaticAgeStrategy;
import src.variant.attack.AttackStrategy;
import src.variant.attack.StaticAttackStrategy;
import src.variant.population.PopulationStrategy;
import src.variant.population.StaticPopulationStrategy;
import src.variant.unitAction.NoUnitActionStrategy;
import src.variant.unitAction.RealUnitActionStrategy;
import src.variant.unitAction.UnitActionStrategy;
import src.variant.win.TimeWinStrategy;
import src.variant.win.WinStrategy;
import src.variant.workforce.StaticProdWorkforceStrategy;
import src.variant.workforce.WorkforceStrategy;
import src.variant.worldGeneration.AlphaWorldGenerationStrategy;
import src.variant.worldGeneration.WorldGenerationStrategy;

public class GammaCivFactory implements CivFactory {
    @Override
    public WorkforceStrategy workForceStrategy() {
        return new StaticProdWorkforceStrategy();
    }

    @Override
    public WinStrategy winStrategy() {
        return new TimeWinStrategy();
    }

    @Override
    public AgeStrategy ageStrategy() {
        return new StaticAgeStrategy();
    }

    @Override
    public UnitActionStrategy unitActionStrategy() {
        return new RealUnitActionStrategy();
    }

    @Override
    public WorldGenerationStrategy worldGenerationStrategy() {
        return new AlphaWorldGenerationStrategy();
    }

    @Override
    public AttackStrategy attackStrategy() {
        return new StaticAttackStrategy();
    }

    @Override
    public PopulationStrategy populationStrategy() {
        return new StaticPopulationStrategy();
    }

    @Override
    public WorkforceStrategy workforceStrategy() {
        return new StaticProdWorkforceStrategy();
    }
}
