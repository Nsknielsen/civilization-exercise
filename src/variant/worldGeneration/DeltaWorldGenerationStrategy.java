package src.variant.worldGeneration;

import src.common.game.Game;
import src.common.game.Player;
import src.common.world.*;

import java.util.HashMap;

public class DeltaWorldGenerationStrategy extends EmptyWorldGenerationStrategy implements WorldGenerationStrategy {


    @Override
    public World generateWorld() {
        return super.generateWorld();
    }

    @Override
    public HashMap<Position, Tile> generateTiles() {

        //NOTE: ignoring the large map of tiles in the exercise for now.
        return new HashMap<>();
    }

    @Override
    public HashMap<Position, City> generateCities() {
        HashMap<Position, City> cities = new HashMap<>();
        //TODO can this be handled with passing position to citybuilder?
        cities.put(new Position(8, 12), new CityImpl(Player.RED, 0, GameConstants.ARCHER, 0, 1, GameConstants.productionFocus));
        cities.put(new Position(4, 5), new CityImpl(Player.BLUE, 0, GameConstants.ARCHER, 0, 1, GameConstants.productionFocus));
        return cities;
    }

    @Override
    public HashMap<Position, Unit> generateUnits() {
        //NOTE: the image in the exercise does not make it clear which unit belongs to which player.
        HashMap<Position, Unit> units = new HashMap<>();
        units.put(new Position(1,1), new UnitImpl(GameConstants.ARCHER, Player.RED, 1, 2, 3));
        return units;
    }
}
