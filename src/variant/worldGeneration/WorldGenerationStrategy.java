package src.variant.worldGeneration;

import src.common.world.*;

import java.util.HashMap;

public interface WorldGenerationStrategy {

    public World generateWorld();

    public HashMap<Position, Tile> generateTiles();

    public HashMap<Position, City> generateCities();

    public HashMap<Position, Unit> generateUnits();
}
