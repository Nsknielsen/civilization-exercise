package src.variant.worldGeneration;

import src.common.world.*;

import java.util.HashMap;

public class EmptyWorldGenerationStrategy implements WorldGenerationStrategy {


    @Override
    public World generateWorld() {
        World world = new WorldImpl();
        world.setTiles(generateTiles());
        world.setCities(generateCities());
        world.setUnits(generateUnits());
        return world;
    }

    @Override
    public HashMap<Position, Tile> generateTiles() {
        return new HashMap<>();
    }

    @Override
    public HashMap<Position, City> generateCities() {
        return new HashMap<>();
    }

    @Override
    public HashMap<Position, Unit> generateUnits() {
        return new HashMap<>();
    }
}
