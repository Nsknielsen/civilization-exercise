package src.variant.worldGeneration;

import src.common.game.Player;
import src.common.world.*;

import java.util.HashMap;

public class AlphaWorldGenerationStrategy extends EmptyWorldGenerationStrategy implements WorldGenerationStrategy {

    @Override
    public World generateWorld() {
        return super.generateWorld();
    }

    @Override
    public HashMap<Position, Tile> generateTiles() {
        HashMap<Position, Tile> tiles = new HashMap<>();
        tiles.put(new Position(1, 0), new TileImpl(GameConstants.OCEANS));
        tiles.put(new Position(0, 1), new TileImpl(GameConstants.HILLS));
        tiles.put(new Position(2, 2), new TileImpl(GameConstants.MOUNTAINS));
        return tiles;
    }

    @Override
    public HashMap<Position, City> generateCities() {
        HashMap<Position, City> cities = new HashMap<>();
        //TODO can this be handled with passing position to citybuilder?
        cities.put(new Position(1, 1), new CityImpl(Player.RED, 0, GameConstants.ARCHER, 0, 1, GameConstants.productionFocus));
        cities.put(new Position(4, 1), new CityImpl(Player.BLUE, 0, GameConstants.ARCHER, 0, 1, GameConstants.productionFocus));
        return cities;
    }

    @Override
    public HashMap<Position, Unit> generateUnits() {
        HashMap<Position, Unit> units = new HashMap<>();
        units.put(new Position(2, 0), new UnitImpl(GameConstants.ARCHER, Player.RED, 1, 2, 3));
        units.put(new Position(3, 2), new UnitImpl(GameConstants.LEGION, Player.BLUE, 1, 4, 2));
        units.put(new Position(4, 3), new UnitImpl(GameConstants.SETTLER, Player.RED, 1, 0, 3));
        return units;
    }
}
