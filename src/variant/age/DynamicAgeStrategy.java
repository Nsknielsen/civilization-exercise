package src.variant.age;

public class DynamicAgeStrategy implements AgeStrategy {


    @Override
    public int increaseGameAge(int currentAge) {

        if (currentAge < -100){
            return currentAge + 100;
        }

        if (currentAge == -100){
            return -1;
        } else if (currentAge == -1){
            return 1;
        } else if (currentAge == 1) {
            return 50;
        }

        int interval;
        if (currentAge >= 50 && currentAge < 1750){
            interval = 50;
        } else if (currentAge >= 1750 && currentAge < 1900) {
            interval = 25;
        } else if (currentAge >= 1900 && currentAge < 1970) {
            interval = 5;
        } else {
            interval = 1;
        }
        return currentAge + interval;
    }
}
