package src.variant.age;

public class StaticAgeStrategy implements AgeStrategy {

    @Override
    public int increaseGameAge(int currentAge) {
        return currentAge + 100;
    }
}
