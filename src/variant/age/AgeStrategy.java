package src.variant.age;

public interface AgeStrategy {


    int increaseGameAge(int currentAge);
}
