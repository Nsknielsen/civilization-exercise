package src.variant.attack;

import src.common.world.Position;
import src.common.world.World;

public interface AttackStrategy {

    boolean resolveAttack(World world, Position from, Position to);
}
