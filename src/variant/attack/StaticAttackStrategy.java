package src.variant.attack;

import src.common.world.Position;
import src.common.world.World;

public class StaticAttackStrategy implements AttackStrategy {
    @Override
    public boolean resolveAttack(World world, Position from, Position to) {
        return true;
    }
}
