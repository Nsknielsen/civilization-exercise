package src.variant.attack;

import src.common.game.Player;
import src.common.world.*;

import java.util.ArrayList;

import static src.common.util.SurroundingPositionsUtil.listOfSurroundingPositions;

public class RealAttackStrategy implements AttackStrategy {

    private final RollStrategy rollStrategy;
    public RealAttackStrategy(RollStrategy rollStrategy){
        this.rollStrategy = rollStrategy;
    }
    @Override
    public boolean resolveAttack(World world, Position from, Position to) {
        int attack = world.getUnitAt(from).getAttackingStrength() + countNeighbours(world, from);
        int defense = world.getUnitAt(to).getDefensiveStrength() + countNeighbours(world, to);

        attack = attack * tileModifier(world.getTileAt(from)) * cityModifier(world.getCityAt(from));
        defense = defense * tileModifier(world.getTileAt(to)) * cityModifier(world.getCityAt(to));

        return attack * rollStrategy.roll() > defense * rollStrategy.roll();
    }

    private int cityModifier(City c) {
        return c != null ? 3 : 1;
    }

    private int tileModifier(Tile tile) {
        return tile.getTypeString().equals(GameConstants.HILLS)  || tile.getTypeString().equals(GameConstants.FOREST)
                ? 2 : 1;
    }

    private int countNeighbours(World world, Position p) {
        Player owner = world.getUnitAt(p).getOwner();
        ArrayList<Position> surroundingPositions = listOfSurroundingPositions(p.getRow(), p.getColumn());
        return (int) surroundingPositions.stream()
                .filter(sp -> world.getUnitAt(sp) != null && world.getUnitAt(sp).getOwner() == owner)
                .count();
    }
}
