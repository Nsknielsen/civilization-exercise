package src.variant.attack;

public interface RollStrategy {

    public int roll();
}
