package src.variant.attack;

import java.util.concurrent.ThreadLocalRandom;

public class RandomRollStrategy implements RollStrategy {
    @Override
    public int roll() {
        return ThreadLocalRandom.current().nextInt(1, 7);
    }
}
