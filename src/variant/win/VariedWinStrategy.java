package src.variant.win;

import src.common.game.GameEventSubject;
import src.common.game.Player;

public class VariedWinStrategy implements WinStrategy {
    private GameEventSubject gameEventSubject;

    private WinStrategy cityCountStrategy;

    private WinStrategy battleVictoryStrategy;

    private WinStrategy state;

    public VariedWinStrategy(){
        this.cityCountStrategy = new CityCountWinStrategy();
        this.battleVictoryStrategy = new BattleVictoryWinStrategy();
        this.state = this.cityCountStrategy;
    }

    @Override
    public Player getWinner() {
        return this.state.getWinner();
    }

    @Override
    public void addEventSubject(GameEventSubject gameEventSubject) {
        this.gameEventSubject = gameEventSubject;
        this.cityCountStrategy.addEventSubject(gameEventSubject);
        this.battleVictoryStrategy.addEventSubject(gameEventSubject);
    }

    @Override
    public void update() {
        if (this.gameEventSubject.getRounds() > 20){
            this.state = this.battleVictoryStrategy;
        }
        this.state.update();
    }


}
