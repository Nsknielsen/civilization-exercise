package src.variant.win;

import src.common.game.GameEventSubject;
import src.common.game.Player;

import java.util.HashMap;

public class BattleVictoryWinStrategy implements WinStrategy {

     private int rounds = 0;

     private HashMap<Player, Integer> attackerWins;



    private GameEventSubject gameEventSubject;
    @Override
    public Player getWinner() {
        //Todo refac this to be general for both players
        if (this.attackerWins.get(Player.RED) >= 3){
            return Player.RED;
        }
        if (this.attackerWins.get(Player.BLUE) >= 3) {
            return Player.BLUE;
        }
        return null;
    }

    @Override
    public void addEventSubject(GameEventSubject gameEventSubject) {
        this.gameEventSubject = gameEventSubject;
        update();
    }

    @Override
    public void update() {
        this.attackerWins = this.gameEventSubject.getAttackerWins();
    }
}
