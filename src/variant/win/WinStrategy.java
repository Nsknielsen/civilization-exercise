package src.variant.win;

import src.common.game.GameEventSubject;
import src.common.game.Player;

public interface WinStrategy {
    Player getWinner();

    void addEventSubject(GameEventSubject gameEventSubject);

    void update();
}
