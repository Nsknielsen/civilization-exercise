package src.variant.win;

import src.common.game.GameEventSubject;
import src.common.game.Player;

public class TimeWinStrategy implements WinStrategy {

    private int gameAge;

    private GameEventSubject gameEventSubject;

    @Override
    public Player getWinner() {
        return this.gameAge >= -3000 ? Player.RED : null;
    }

    @Override
    public void addEventSubject(GameEventSubject gameEventSubject) {
        this.gameEventSubject = gameEventSubject;
    }

    @Override
    public void update() {
        this.gameAge = this.gameEventSubject.getGameAge();
    }

}
