package src.variant.win;

import src.common.game.Player;
import src.common.world.City;
import src.common.game.GameEventSubject;
import src.common.world.Position;

import java.util.HashMap;

public class CityCountWinStrategy implements WinStrategy {

    private GameEventSubject gameEventSubject;

    private HashMap<Position, City> cities;

    @Override
    public Player getWinner() {

        long redCities = cities.entrySet().stream().filter((entry -> entry.getValue().getOwner() == Player.RED)).count();
        long blueCities = cities.entrySet().stream().filter((entry -> entry.getValue().getOwner() == Player.BLUE)).count();
        if (redCities == cities.size()){
            return Player.RED;
        } else if (blueCities == cities.size()) {
           return Player.BLUE;
        }
        return null;
    }

    @Override
    public void addEventSubject(GameEventSubject gameEventSubject) {
        this.gameEventSubject = gameEventSubject;
    }

    @Override
    public void update() {
    this.cities = this.gameEventSubject.getCities();
    }

}
