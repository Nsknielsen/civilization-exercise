package src.variant.unitAction;

import src.common.dto.UnitEventObject;
import src.common.world.City;
import src.common.world.Unit;

public interface UnitActionStrategy {

    public UnitEventObject performUnitAction(City city, Unit unit);

    boolean isAllowedToMove(Unit u );
}
