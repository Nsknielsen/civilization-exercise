package src.variant.unitAction;

import src.common.dto.UnitEventObject;
import src.common.dto.UnitEventObjectImpl;
import src.common.world.City;
import src.common.world.Unit;

public class NoUnitActionStrategy implements UnitActionStrategy {

    @Override
    public UnitEventObject performUnitAction(City city, Unit unit) {
        return new UnitEventObjectImpl(city, unit);

    }

    @Override
    public boolean isAllowedToMove(Unit u) {
        return true;
    }

}
