package src.variant.unitAction;

import src.common.dto.UnitEventObject;
import src.common.dto.UnitEventObjectImpl;
import src.common.world.*;

public class RealUnitActionStrategy implements UnitActionStrategy {

    @Override
    public UnitEventObject performUnitAction(City city, Unit unit) {
        switch (unit.getTypeString()){
            case GameConstants.ARCHER: return archerAction(city, unit);
            case GameConstants.SETTLER: return settlerAction(city, unit) ;
            default: return new UnitEventObjectImpl(city, unit);
        }
    }

    @Override
    public boolean isAllowedToMove(Unit u) {
        return u.getTypeString().equals(GameConstants.ARCHER ) && u.getDefensiveStrength() == 6
                ? false : true;
    }

    private UnitEventObject settlerAction(City c, Unit u){
        if (c != null){
            return new UnitEventObjectImpl(c, u);
        }
        return new UnitEventObjectImpl(new CityImpl(u.getOwner(), 0, GameConstants.ARCHER, 0, 0, GameConstants.productionFocus), null);
    }
    private UnitEventObject archerAction(City c, Unit u) {
        int newDefense = u.getDefensiveStrength() == 3 ? u.getDefensiveStrength() * 2 : u.getDefensiveStrength() / 2;
        return new UnitEventObjectImpl(c, new UnitImpl(u.getTypeString(), u.getOwner(), u.getMoveCount()
        , u.getAttackingStrength(), newDefense));
    }

}
