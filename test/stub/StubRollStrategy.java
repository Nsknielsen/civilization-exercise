package test.stub;

import src.variant.attack.RollStrategy;

import java.util.ArrayList;

public class StubRollStrategy implements RollStrategy {

    ArrayList<Integer> values;

    public StubRollStrategy(ArrayList<Integer> values){ this.values = values;}
    @Override
    public int roll() {
        return this.values.size() > 0 ? this.values.remove(0) : 0;
    }
}
