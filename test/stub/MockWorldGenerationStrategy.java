package test.stub;

import src.common.game.Player;
import src.common.world.*;
import src.variant.worldGeneration.EmptyWorldGenerationStrategy;
import src.variant.worldGeneration.WorldGenerationStrategy;

import java.util.HashMap;

public class MockWorldGenerationStrategy extends EmptyWorldGenerationStrategy implements WorldGenerationStrategy {


    @Override
    public World generateWorld() {
        return super.generateWorld();
    }

    @Override
    public HashMap<Position, Tile> generateTiles() {
        return new HashMap<>();
    }

    @Override
    public HashMap<Position, City> generateCities() {
        HashMap<Position, City> cities = new HashMap<>();
        cities.put(new Position(1, 1), new CityImpl(Player.RED, 0, GameConstants.ARCHER, 0, 0, GameConstants.productionFocus));
        return cities;
    }

    @Override
    public HashMap<Position, Unit> generateUnits() {
        HashMap<Position, Unit> units = new HashMap<>();
        units.put(new Position(2, 0), new UnitImpl(GameConstants.ARCHER, Player.RED, 1, 2, 3));
        units.put(new Position(3, 2), new UnitImpl(GameConstants.LEGION, Player.BLUE, 1, 4, 2));
        units.put(new Position(4, 3), new UnitImpl(GameConstants.SETTLER, Player.RED, 1, 0, 3));
        return units;
    }
}
