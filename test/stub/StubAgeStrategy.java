package test.stub;

import src.variant.age.AgeStrategy;

public class StubAgeStrategy implements AgeStrategy {
    @Override
    public int increaseGameAge(int currentAge) {
        return currentAge;
    }
}
