package test.stub;

import src.common.game.GameEventSubject;
import src.common.game.Player;
import src.variant.win.WinStrategy;

public class StubWinStrategy implements WinStrategy {

    private GameEventSubject gameEventSubject;
    @Override
    public Player getWinner() {
        return null;
    }

    @Override
    public void addEventSubject(GameEventSubject gameEventSubject) {

        this.gameEventSubject = gameEventSubject;
    }

    @Override
    public void update() {

    }
}
