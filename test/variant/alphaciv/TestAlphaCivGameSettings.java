package test.variant.alphaciv;

import org.junit.*;
import src.common.game.Game;
import src.common.game.GameImpl;
import src.common.game.Player;
import src.common.world.GameConstants;
import src.common.world.Position;
import src.variant.factory.AlphaCivFactory;
import src.variant.workforce.StaticProdWorkforceStrategy;

import static org.junit.Assert.*;

/** Skeleton class for AlphaCiv test cases 

   This source code is from the book 
     "Flexible, Reliable Software:
       Using Patterns and Agile Development"
     published by CRC Press.
   Author: 
     Henrik B Christensen 
     Department of Computer Science
     Aarhus University
   
   Please visit http://www.baerbak.com/ for further information.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
 
       http://www.apache.org/licenses/LICENSE-2.0
 
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/
public class TestAlphaCivGameSettings {
  private Game game;
  /** Fixture for alphaciv testing. */
  @Before
  public void setUp() {
    game = new GameImpl(new AlphaCivFactory()
    );
  }

  @Test public void shouldAge100PerRound(){
    game.endOfTurn();
    game.endOfTurn();
    assertEquals(-3900, game.getAge());
  }

  @Test public void shouldBeRedWinnerBy3000Bc(){
    game.endOfTurn();
    game.endOfTurn();
    game.endOfTurn();
    game.endOfTurn();
    game.endOfTurn();
    game.endOfTurn();
    game.endOfTurn();
    game.endOfTurn();
    game.endOfTurn();
    game.endOfTurn();
    game.endOfTurn();
    game.endOfTurn();
    game.endOfTurn();
    game.endOfTurn();
    game.endOfTurn();
    game.endOfTurn();
    game.endOfTurn();
    game.endOfTurn();
    game.endOfTurn();
    game.endOfTurn();
    assertEquals(-3000, game.getAge());
    assertEquals(Player.RED, game.getWinner());
  }

  @Test public void shouldHaveNoUnitAction(){
    game.performUnitActionAt(new Position(2,0));
    assertEquals(3, game.getUnitAt(new Position(2,0)).getDefensiveStrength());
  }

  @Test public void shouldHaveStaticWorkforceFocus(){
    game.changeWorkForceFocusInCityAt(new Position(1, 1), GameConstants.foodFocus);
    assertEquals(GameConstants.productionFocus, game.getCityAt(new Position(1, 1)).getWorkforceFocus());
  }
}