package test.variant.alphaciv;

import org.junit.Before;
import org.junit.Test;
import src.common.game.Game;
import src.common.game.GameImpl;
import src.common.game.Player;
import src.common.world.City;
import src.common.world.GameConstants;
import src.common.world.Position;
import src.common.world.Unit;
import src.variant.factory.AlphaCivFactory;
import src.variant.workforce.StaticProdWorkforceStrategy;

import static org.junit.Assert.*;

public class TestAlphaCivWorldGeneration {

    private Game game;
    @Before
    public void setUp() {
        game = new GameImpl(new AlphaCivFactory()
        );
    }


    @Test
    public void shouldHaveRedCityAt1_1() {
        City c = game.getCityAt(new Position(1,1));
        assertNotNull("There should be a city at (1,1)", c);
        Player p = c.getOwner();
        assertEquals( "src.common.world.City at (1,1) should be owned by red",
                Player.RED, p );
    }

    @Test
    public void shouldHaveBlueCityAt4_1(){
        City c = game.getCityAt(new Position(4, 1));
        assertNotNull("There should be a city at (4,1)", c);
        Player p = c.getOwner();
        assertEquals( "src.common.world.City at (4,1) should be owned by blue",
                Player.BLUE, p );
    }

    @Test public void shouldBeOceanAt1_0(){
        assertEquals(game.getTileAt(new Position(1, 0)).getTypeString(), GameConstants.OCEANS);
    }

    @Test public void shouldBeHillsAt0_1(){
        assertEquals(game.getTileAt(new Position(0, 1)).getTypeString(), GameConstants.HILLS);
    }

    @Test public void shouldBeMountainsAt02_2(){
        assertEquals(game.getTileAt(new Position(2, 2)).getTypeString(), GameConstants.MOUNTAINS);
    }

    @Test public void shouldBePlainsElsewhere(){
        assertEquals(game.getTileAt(new Position(6, 8)).getTypeString(), GameConstants.PLAINS);
    }

    @Test public void shouldHaveRedArcherAt2_0(){
        Unit u = game.getUnitAt(new Position(2, 0));
        assertEquals(u.getTypeString(), GameConstants.ARCHER);
        Player p = u.getOwner();
        assertEquals(Player.RED, p);
    }

    @Test public void shouldHaveBlueLegionAt3_2(){
        Unit u = game.getUnitAt(new Position(3, 2));
        assertEquals(u.getTypeString(), GameConstants.LEGION);
        Player p = u.getOwner();
        assertEquals(Player.BLUE, p);
    }

    @Test public void shouldHaveRedSettlerAt4_3(){
        Unit u = game.getUnitAt(new Position(4, 3));
        assertEquals(u.getTypeString(), GameConstants.SETTLER);
        Player p = u.getOwner();
        assertEquals(Player.RED, p);
    }

    @Test public void shouldHaveNoUnitAt11_9(){
        assertNull( game.getUnitAt(new Position(11, 9)));
    }
}
