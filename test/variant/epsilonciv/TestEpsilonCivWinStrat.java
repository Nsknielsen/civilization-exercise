package test.variant.epsilonciv;


import org.junit.Test;
import src.common.game.Game;
import src.common.game.GameImpl;
import src.common.game.Player;
import src.common.world.GameConstants;
import src.common.world.Position;
import src.variant.factory.EpsilonCivFactory;
import src.variant.workforce.StaticProdWorkforceStrategy;
import test.stub.StubRollStrategy;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.*;

public class TestEpsilonCivWinStrat {

    private Game game;

    private Game makeGame(ArrayList<Integer> rolls){
        return new GameImpl(new EpsilonCivFactory(new StubRollStrategy(rolls))
        );
    }

    @Test public void shouldBeNoWinnerAtFirst(){
        game = makeGame(new ArrayList<>(Arrays.asList(1,1)));
        assertNull(game.getWinner());
    }

   @Test public void shouldWinAfter3AttackVictories(){
        game = makeGame(new ArrayList<>(Arrays.asList(6, 1, 6, 1, 6, 1)));
        game.moveUnit(new Position(2, 0), new Position(2,1));
        game.endOfTurn(); //now blue
        game.moveUnit(new Position(3,2), new Position(2,1)); //win 1
        game.endOfTurn(); //now red
        game.endOfTurn(); // now blue
        game.endOfTurn(); //now red
        assertEquals(GameConstants.ARCHER, game.getUnitAt(new Position(1,1)).getTypeString());
        game.moveUnit(new Position(1,1 ), new Position(2,0));
        game.endOfTurn(); // now blue
        game.moveUnit(new Position(2, 1), new Position(2, 0)); // win 2
        game.endOfTurn(); // now red
        game.endOfTurn(); // now blue
        game.endOfTurn(); // now red
        assertEquals(GameConstants.ARCHER, game.getUnitAt(new Position(1,1)).getTypeString());
        game.moveUnit(new Position(1,1), new Position(2,1));
        game.endOfTurn(); // now blue
        game.moveUnit(new Position(2, 0), new Position(2, 1)); // win 3
        assertEquals(GameConstants.LEGION, game.getUnitAt(new Position(2,1)).getTypeString());
        assertEquals(Player.BLUE, game.getWinner());
    }

}
