package test.variant.epsilonciv;

import org.junit.Test;
import src.common.game.Game;
import src.common.game.GameImpl;
import src.common.game.Player;
import src.common.world.GameConstants;
import src.common.world.Position;
import src.variant.factory.EpsilonCivFactory;
import src.variant.workforce.StaticProdWorkforceStrategy;
import test.stub.StubRollStrategy;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.*;

public class TestEpsilonCivAttacks {

    private Game game;

    private Game makeGame(ArrayList<Integer> rolls){
        return new GameImpl(new EpsilonCivFactory(new StubRollStrategy(rolls))
        );
    }


    @Test public void shouldWinAndMoveWhenAttackIsOverDefense(){
        game = makeGame(new ArrayList<>(Arrays.asList(1, 1)));
        game.moveUnit(new Position(2, 0), new Position(2, 1));
        game.endOfTurn();
        game.moveUnit(new Position(3, 2), new Position(2,1));
        assertEquals(Player.BLUE, game.getUnitAt(new Position(2, 1)).getOwner());
    }

    @Test public void shouldWinAndStayWhenDefOverAtt(){
        game = makeGame(new ArrayList<>(Arrays.asList(1, 3)));
        game.moveUnit(new Position(2, 0), new Position(2, 1));
        game.endOfTurn();
        game.moveUnit(new Position(3, 2), new Position(2,1));
        assertEquals(GameConstants.ARCHER, game.getUnitAt(new Position(2, 1)).getTypeString());
    }

    @Test public void shouldWinDefWhenEvenScores(){
        game = makeGame(new ArrayList<>(Arrays.asList(1, 2)));
        game.moveUnit(new Position(2, 0), new Position(2, 1));
        game.endOfTurn();
        game.moveUnit(new Position(3, 2), new Position(2,1));
        assertEquals(GameConstants.ARCHER, game.getUnitAt(new Position(2, 1)).getTypeString());
    }

    @Test public void shouldMultiplyBy2OnForest(){
        game = makeGame(new ArrayList<>(Arrays.asList(2, 1)));
        game.moveUnit(new Position(2, 0), new Position(1, 1));
        game.endOfTurn();
        game.moveUnit(new Position(3, 2), new Position(2,1));
        game.endOfTurn();
        game.moveUnit(new Position(1,1), new Position(0, 1));
        game.endOfTurn();
        game.moveUnit(new Position(2,1), new Position(3,2));
        game.endOfTurn();
        game.endOfTurn();
        assertEquals(GameConstants.LEGION, game.getUnitAt(new Position(3, 2)).getTypeString());
        game.moveUnit(new Position(3, 2), new Position(0,1));
        assertEquals(GameConstants.ARCHER, game.getUnitAt(new Position(0,1)).getTypeString());
    }

    @Test public void shouldMultiplyBy3InCity(){
        game = makeGame(new ArrayList<>(Arrays.asList(2, 1)));
        game.moveUnit(new Position(2, 0), new Position(1, 1));
        game.endOfTurn();
        game.moveUnit(new Position(3, 2), new Position(2,1));
        game.endOfTurn();
        assertEquals(GameConstants.ARCHER, game.getUnitAt(new Position(1,1)).getTypeString());
        game.endOfTurn();
        game.moveUnit(new Position(2,1), new Position(1,1));
        assertEquals(GameConstants.ARCHER, game.getUnitAt(new Position(1,1)).getTypeString());
    }

    @Test public void shouldGet1BonusPerNeighbour(){
        game = makeGame(new ArrayList<>(Arrays.asList(2, 1)));
        game.moveUnit(new Position(2, 0), new Position(1, 1));
        game.endOfTurn();
        game.moveUnit(new Position(3, 2), new Position(2,1));
        game.endOfTurn();
        game.moveUnit(new Position(1,1), new Position(0,1));
        game.endOfTurn();
        game.moveUnit(new Position(2, 1), new Position(1, 2));
        game.endOfTurn();
        game.endOfTurn();
        game.moveUnit(new Position(1, 2), new Position(0, 1));
        assertEquals(GameConstants.ARCHER, game.getUnitAt(new Position(0, 1)).getTypeString());
    }
}
