package test.variant.etaciv;

import org.junit.Before;
import org.junit.Test;
import src.common.game.Game;
import src.common.game.GameImpl;
import src.common.game.Player;
import src.common.world.GameConstants;
import src.common.world.Position;
import src.variant.factory.EtaCivFactory;
import src.variant.workforce.VariedWorkforceStrategy;

import static org.junit.Assert.assertEquals;

public class TestEtaCivPopulationStrategy {

    private Game game;
    @Before
    public void setUp() {
        game = new GameImpl(new EtaCivFactory()
        );
    }

    @Test
    public void shouldGrowTo2PopulationAtOver8Food(){
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        assertEquals(8, game.getCityAt(new Position(1,1)).getFood());
        game.endOfTurn();
        game.endOfTurn();
        assertEquals(0, game.getCityAt(new Position(1,1)).getFood());
        assertEquals(2, game.getCityAt(new Position(1,1)).getPopulation());
    }

    @Test public void shouldKeepPopulationWhenCaptured(){
        game.changeProductionInCityAt(new Position(1,1), GameConstants.SETTLER);
        game.endOfTurn();
        game.moveUnit(new Position(4, 3), new Position(3, 2));
        game.endOfTurn();
        game.endOfTurn();
        game.moveUnit(new Position(3,2), new Position(2,1));
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.moveUnit(new Position(2,1), new Position(1,1));
        assertEquals(Player.BLUE, game.getCityAt(new Position(1,1)).getOwner());
        assertEquals(2, game.getCityAt(new Position(1,1)).getPopulation());
    }

}
