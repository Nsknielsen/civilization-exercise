package test.variant.etaciv;

import org.junit.Before;
import org.junit.Test;
import src.common.game.Game;
import src.common.game.GameImpl;
import src.common.world.GameConstants;
import src.common.world.Position;
import src.variant.factory.EtaCivFactory;
import src.variant.workforce.VariedWorkforceStrategy;

import static org.junit.Assert.assertEquals;

public class TestEtaCivPopWorkforceIntegration {
    private Game game;
    @Before
    public void setUp() {
        game = new GameImpl(new EtaCivFactory()
        );
    }

    @Test
    public void shouldProdFocusSecondPopOnHillFor3ProdPerTurn(){
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        assertEquals(9, game.getCityAt(new Position(1,1)).getTreasury());
        game.changeProductionInCityAt(new Position(1,1), GameConstants.SETTLER);
        game.endOfTurn();
        game.endOfTurn();
        assertEquals(12, game.getCityAt(new Position(1,1)).getTreasury());
    }

    @Test public void shouldFoodFocusSecondPopOnPlainsFor4FoodPerTurn(){
        game.changeWorkForceFocusInCityAt(new Position(1,1), GameConstants.foodFocus);
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        assertEquals(0, game.getCityAt(new Position(1,1)).getFood());
        assertEquals(2, game.getCityAt(new Position(1,1)).getPopulation());
        game.endOfTurn();
        game.endOfTurn();
        assertEquals(4, game.getCityAt(new Position(1,1)).getFood());
    }

    @Test public void shouldFoodFocus2OnPlainsWith3PopFor7Food(){
        game.changeWorkForceFocusInCityAt(new Position(1,1), GameConstants.foodFocus);
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        assertEquals(0, game.getCityAt(new Position(1,1)).getFood());
        assertEquals(3, game.getCityAt(new Position(1,1)).getPopulation());
        game.endOfTurn();
        game.endOfTurn();
        assertEquals(7, game.getCityAt(new Position(1,1)).getFood());
    }

}
