package test.variant.betaciv;

import org.junit.*;
import src.common.game.Game;
import src.common.game.GameImpl;
import src.variant.factory.BetaCivFactory;
import src.variant.workforce.StaticProdWorkforceStrategy;

import static org.junit.Assert.*;

public class TestBetaCivAgeStrategy {

    private Game game;
    @Before
    public void setUp(){game = new GameImpl(new BetaCivFactory()
    );}

    @Test public void shouldAge100YearsAtFirst(){
        game.endOfTurn();
        game.endOfTurn();
        assertEquals(-3900, game.getAge());
    }
}
