package test.variant.betaciv;

import org.junit.Before;
import org.junit.Test;
import src.common.game.Game;
import src.common.game.GameImpl;
import src.common.game.Player;
import src.common.world.GameConstants;
import src.common.world.Position;
import src.variant.factory.BetaCivFactory;
import src.variant.workforce.StaticProdWorkforceStrategy;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class TestBetaCivWinStrategy {
    private Game game;
    @Before
    public void setUp(){game = new GameImpl(new BetaCivFactory()
    );}

    @Test
    public void shouldHaveNullWinnerAtStart(){
        assertNull(game.getWinner());
    }

    @Test public void shouldWinIfControllingAllCities(){
        game.moveUnit(new Position(2, 0), new Position(3, 0));
        game.endOfTurn();
        game.endOfTurn();
        game.moveUnit(new Position(3, 0), new Position(4, 1));
        assertEquals(GameConstants.ARCHER, game.getUnitAt(new Position(4, 1)).getTypeString());
        assertEquals(Player.RED, game.getCityAt(new Position(4, 1)).getOwner());
        assertEquals(Player.RED, game.getWinner());
    }

}
