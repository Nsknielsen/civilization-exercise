package test.variant.gammaciv;

import org.junit.Before;
import src.common.game.Game;
import src.common.game.GameImpl;
import org.junit.*;
import src.common.game.Player;
import src.common.world.GameConstants;
import src.common.world.Position;
import src.variant.factory.GammaCivFactory;
import src.variant.workforce.StaticProdWorkforceStrategy;

import static org.junit.Assert.*;

public class TestGammaCivUnitActions {

    private Game game;

    @Before
    public void setUp() {
        game = new GameImpl(new GammaCivFactory()
        );
    }

    @Test public void shouldFortifyArcher(){
        assertEquals(GameConstants.ARCHER, game.getUnitAt(new Position(2,0)).getTypeString());
        game.performUnitActionAt(new Position(2, 0));
        game.endOfTurn();
        game.endOfTurn();
        assertEquals(6, game.getUnitAt(new Position(2, 0)).getDefensiveStrength());
    }

    @Test public void fortifiedArcherShouldNotMoveButRetainMovePoints(){
        assertEquals(GameConstants.ARCHER, game.getUnitAt(new Position(2,0)).getTypeString());
        game.performUnitActionAt(new Position(2, 0));
        game.endOfTurn();
        game.endOfTurn();
        assertEquals(1, game.getUnitAt(new Position(2, 0)).getMoveCount());
        assertEquals(GameConstants.ARCHER, game.getUnitAt(new Position(2, 0)).getTypeString());
    }

     @Test public void shouldRemoveSettlerWhenSettling(){
        game.performUnitActionAt(new Position(4, 3));
        assertNull(game.getUnitAt(new Position(4, 3)));
    }

    @Test public void shouldAddCityWhenSettling(){
        game.performUnitActionAt(new Position(4, 3));
        assertEquals(Player.RED, game.getCityAt(new Position(4, 3)).getOwner());
    }

    @Test public void shouldDefortifyArcher(){
        assertEquals(GameConstants.ARCHER, game.getUnitAt(new Position(2,0)).getTypeString());
        game.performUnitActionAt(new Position(2, 0));
        game.performUnitActionAt(new Position(2, 0));
        game.moveUnit(new Position(2, 0), new Position(1, 1));
        assertEquals(GameConstants.ARCHER, game.getUnitAt(new Position(1,1)).getTypeString());
    }

    @Test public void shouldNotSettleOnExistingCity(){
        game.moveUnit(new Position(4, 3), new Position(4, 2));
        game.endOfTurn();
        game.endOfTurn();
        game.moveUnit(new Position(4, 2), new Position(4, 1));
        assertEquals(Player.RED, game.getCityAt(new Position(4, 1)).getOwner());
        game.performUnitActionAt(new Position(4, 1));
        assertEquals(GameConstants.SETTLER, game.getUnitAt(new Position(4, 1)).getTypeString());
    }


}
