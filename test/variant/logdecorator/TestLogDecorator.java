package test.variant.logdecorator;

import src.common.game.Game;
import src.common.game.GameImpl;
import org.junit.*;
import src.common.util.LogDecorator;
import src.common.world.GameConstants;
import src.common.world.Position;
import src.variant.factory.SemiCivFactory;

public class TestLogDecorator {

    private Game logger;
    @Before
    public void setUp() {
        logger = new LogDecorator(new GameImpl(new SemiCivFactory())
        );
    }

    @Test public void shouldLogTurnEnd(){
        logger.endOfTurn();
    }

    @Test public void shouldLogWorkforceChange(){
        logger.changeWorkForceFocusInCityAt(new Position(8, 12), GameConstants.productionFocus);
    }

    @Test public void shouldLogUnitMovement(){
        logger.moveUnit(new Position(1,1), new Position(1,2));
    }

    @Test public void shouldLogProductionChange(){
        logger.changeProductionInCityAt(new Position(8, 12), GameConstants.LEGION);
    }

    @Test public void shouldLogUnitAction(){
        logger.performUnitActionAt(new Position(1,1));
    }

    @Test public void shouldLogCurrentWinnerWhenPrompted(){
        logger.getWinner();
    }
}
