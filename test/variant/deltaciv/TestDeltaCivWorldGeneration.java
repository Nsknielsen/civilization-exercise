package test.variant.deltaciv;

import org.junit.Before;
import src.common.game.Game;
import src.common.game.GameImpl;
import org.junit.*;
import src.common.game.Player;
import src.common.world.Position;
import src.variant.factory.DeltaCivFactory;
import src.variant.workforce.StaticProdWorkforceStrategy;

import static org.junit.Assert.*;

public class TestDeltaCivWorldGeneration {

    private Game game;

    @Before
    public void setUp() {
        game = new GameImpl(new DeltaCivFactory()
        );
    }

    @Test public void shouldHaveRedCityAt8_12(){
        assertEquals(Player.RED, game.getCityAt(new Position(8, 12)).getOwner());
    }

    @Test public void shouldHaveBlueCityAt4_5(){
        assertEquals(Player.BLUE, game.getCityAt(new Position(4,5)).getOwner());
    }




}
