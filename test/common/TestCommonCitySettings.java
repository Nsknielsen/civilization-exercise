package test.common;

import org.junit.Before;
import org.junit.Test;
import src.common.game.Game;
import src.common.world.GameConstants;
import src.common.game.GameImpl;
import src.common.world.Position;
import src.variant.workforce.StaticProdWorkforceStrategy;
import test.stub.StubCivFactory;

import static org.junit.Assert.assertEquals;

public class TestCommonCitySettings {

    private Game game;

    @Before
    public void setUp() {
        game = new GameImpl(new StubCivFactory()
        );
    }

    @Test
    public void shouldHave1Population(){
        assertEquals(1, game.getCityAt(new Position(1,1)).getSize());
    }

    @Test public void shouldHaveProductionFocusNotFood(){
        assertEquals(GameConstants.productionFocus, game.getCityAt(new Position(1,1)).getWorkforceFocus());
    }

    @Test public void shouldHave0ProductionStockAtCity(){
        assertEquals(0, game.getCityAt(new Position(1, 1)).getTreasury());
    }

    @Test public void shouldadd6ProductionStockAtCityPerRound(){
        game.endOfTurn();
        assertEquals(0, game.getCityAt(new Position(1, 1)).getTreasury());
        game.endOfTurn();
        assertEquals(6, game.getCityAt(new Position(1, 1)).getTreasury());
    }

    @Test public void shouldBeAbleToSetProductionFocusToArcher(){
        game.changeProductionInCityAt(new Position(1, 1), GameConstants.ARCHER);
        assertEquals(GameConstants.ARCHER, game.getCityAt(new Position(1, 1)).getProduction());
    }

    @Test public void shouldBeAbleToSwitchProductionFocus(){
        game.changeProductionInCityAt(new Position(1, 1), GameConstants.ARCHER);
        assertEquals(GameConstants.ARCHER, game.getCityAt(new Position(1, 1)).getProduction());
        game.changeProductionInCityAt(new Position(1, 1), GameConstants.LEGION);
        assertEquals(GameConstants.LEGION, game.getCityAt(new Position(1, 1)).getProduction());
    }

    @Test public void shouldMakeArcherIfCanAfford(){
        game.changeProductionInCityAt(new Position(1, 1), GameConstants.ARCHER);
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        assertEquals(2, game.getCityAt(new Position(1, 1)).getTreasury());
        assertEquals(GameConstants.ARCHER, game.getUnitAt(new Position(1, 1)).getTypeString());
    }

    @Test public void shouldMakeLegionIfCanAfford(){
        game.changeProductionInCityAt(new Position(1, 1), GameConstants.LEGION);
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        assertEquals(GameConstants.LEGION, game.getUnitAt(new Position(1, 1)).getTypeString());
        assertEquals(3, game.getCityAt(new Position(1, 1)).getTreasury());
    }

    @Test public void shouldMakeUnitNorthIfBlocked(){
        game.changeProductionInCityAt(new Position(1, 1), GameConstants.ARCHER);
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        assertEquals(GameConstants.ARCHER, game.getUnitAt(new Position(1, 1)).getTypeString());
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        assertEquals(GameConstants.ARCHER, game.getUnitAt(new Position(0, 1)).getTypeString());
    }

    @Test public void shouldMakeUnitsClockWiseIfBlocked(){
        game.changeProductionInCityAt(new Position(1, 1), GameConstants.ARCHER);
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        assertEquals(GameConstants.ARCHER, game.getUnitAt(new Position(1, 1)).getTypeString());
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        assertEquals(GameConstants.ARCHER, game.getUnitAt(new Position(0, 1)).getTypeString());
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        assertEquals(GameConstants.ARCHER, game.getUnitAt(new Position(0, 2)).getTypeString());
    }
}
