package test.common;

import org.junit.*;
import src.common.game.Game;
import src.common.game.GameImpl;
import src.common.game.Player;
import src.common.world.GameConstants;
import src.common.world.Position;
import src.common.world.Unit;
import src.variant.workforce.StaticProdWorkforceStrategy;
import test.stub.StubCivFactory;

import static org.junit.Assert.*;

public class TestCommonUnitMovement {

    private Game game;

    @Before
    public void setUp() {
        game = new GameImpl(new StubCivFactory()
        );
    }

    @Test public void shouldMoveRedArcherVertiallyTo3_0(){
        game.moveUnit(new Position(2, 0), new Position(3, 0));
        Unit u = game.getUnitAt(new Position(3, 0));
        assertEquals(u.getTypeString(), GameConstants.ARCHER);
        assertEquals(0, u.getMoveCount());
    }

    @Test public void shouldMoveRedArcherHorizontallyTo2_1(){
        game.moveUnit(new Position(2, 0), new Position(2, 1));
        Unit u = game.getUnitAt(new Position(2, 1));
        assertEquals(u.getTypeString(), GameConstants.ARCHER);
        assertEquals(0, u.getMoveCount());
    }

    @Test public void shouldMoveRedArcherDiagonallyTo1_3(){
        game.moveUnit(new Position(2, 0), new Position(3, 1));
        Unit u = game.getUnitAt(new Position(3, 1));
        assertEquals(u.getTypeString(), GameConstants.ARCHER);
        assertEquals(0, u.getMoveCount());
    }

    @Test public void shouldNotLeaveBoard(){
        game.moveUnit(new Position(2, 0), new Position(2, -1));
        Unit u = game.getUnitAt(new Position(2, 0));
        assertEquals(u.getTypeString(), GameConstants.ARCHER);
    }

    @Test public void shouldExpend1MoveCount(){
        Unit u = game.getUnitAt(new Position(2, 0));
        assertEquals(1, u.getMoveCount());
        game.moveUnit(new Position(2, 0), new Position(3, 0));
        Unit y = game.getUnitAt(new Position(3, 0));
        assertEquals(0, y.getMoveCount());
    }

    @Test public void shouldNotMoveOnNoMoveTiles(){
        Unit u = game.getUnitAt(new Position(2, 0));
        assertEquals(1, u.getMoveCount());
        game.moveUnit(new Position(2, 0), new Position(1, 0));
        assertEquals(1, u.getMoveCount());
    }

    @Test public void shouldNotMoveIfZeroMoveCount(){
        game.moveUnit(new Position(2, 0), new Position(3, 0));
        Unit y = game.getUnitAt(new Position(3, 0));
        assertEquals(0, y.getMoveCount());
        game.moveUnit(new Position(3, 0), new Position(4, 0));
        assertNull( game.getUnitAt(new Position(4, 0)));
    }

    @Test public void shouldOnlyMove1Step(){
        Unit u = game.getUnitAt(new Position(2, 0));
        assertEquals(1, u.getMoveCount());
        game.moveUnit(new Position(2, 0), new Position(5, 0));
        assertEquals(GameConstants.ARCHER, game.getUnitAt(new Position(2, 0)).getTypeString());
    }

    @Test public void shouldNotMoveOtherPlayersUnit(){
        game.moveUnit(new Position(3, 2), new Position(3, 3));
        assertEquals(GameConstants.LEGION, game.getUnitAt(new Position(3, 2)).getTypeString());
    }

    @Test public void shouldEraseOpponentUnitWhenAttacking(){
        game.moveUnit(new Position(2, 0), new Position(2, 1));
        game.endOfTurn();
        game.moveUnit(new Position(3, 2), new Position(2,1 ));
        assertEquals(Player.BLUE, game.getUnitAt(new Position(2, 1)).getOwner());
    }

    @Test public void shouldNotMoveUntoOwnUnit(){
        game.moveUnit(new Position(2, 0), new Position(3, 1));
        game.endOfTurn();
        game.endOfTurn();
        game.moveUnit(new Position(3, 1), new Position(3, 2));
        game.moveUnit(new Position(4, 3), new Position(3, 2));
        assertEquals(GameConstants.ARCHER, game.getUnitAt(new Position(3, 2)).getTypeString());
        assertEquals(GameConstants.SETTLER, game.getUnitAt(new Position(4, 3)).getTypeString());
    }

}
