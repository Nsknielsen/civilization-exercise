package test.common;

import org.junit.*;
import src.common.game.Game;
import src.common.game.GameImpl;
import src.common.game.Player;
import src.variant.workforce.StaticProdWorkforceStrategy;
import test.stub.StubCivFactory;

import static org.junit.Assert.*;

public class TestCommonGameSettings {

    private Game game;

    @Before
    public void setUp() {
        game = new GameImpl(new StubCivFactory()
        );
    }

    @Test public void shouldBeRedInTurnFirst(){
        assertEquals(game.getPlayerInTurn(), Player.RED);
    }

    @Test public void shouldBeBlueInTurnSecond(){
        game.endOfTurn();
        assertEquals(game.getPlayerInTurn(), Player.BLUE);
    }

    @Test public void shouldBeRedInTurnThird(){
        game.endOfTurn();
        game.endOfTurn();
        assertEquals(game.getPlayerInTurn(), Player.RED);
    }

    @Test public void shouldStartAt4000Bc(){
        assertEquals(-4000, game.getAge());
    }

    @Test public void shouldBeNoWinnerAtFirst(){
        assertNull( game.getWinner());
    }

}
