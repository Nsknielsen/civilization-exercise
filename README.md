# Purpose of the project
This is an exercise in test-driven development and design patterns from the book "Flexible, Reliable Software" by Henrik Bærbak Christensen, based on Civilization-style games. Starter code by HBC (https://baerbak.com/).
The project is intentionally developed in a sometimes naive Test-driven approach, ignoring what may seem like obvious improvements in order to follow the flow of the exercise where concepts like design patterns are not introduced in the first version.

Since this is based on older starter code, various starter elements like the 4.4 version of Junit have been left in the project for ease of use.

# How to use 
Open the project in your IDE of choice and use the built-in run command. Alternatively, compile manually in a terminal with the `javac` command and run, for instance, the test suites in `test.variant.*.java` with the `java` command.
